#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sqlalchemy import create_engine

engine = create_engine('postgresql://postgres:geologia@localhost:5432/postgres')
source = "data_.csv"
nrows = None

def df_to_sql(source, engine, nrows=None):
    df = pd.read_csv(source, sep=';', encoding='utf-8', thousands='.', decimal=',', nrows=nrows)
    df.rename(columns=lambda x: x.strip(), inplace=True)
    df = df.apply(lambda x: x.str.strip() if x.dtype == "object" and x is not np.NaN else x)
    df.replace('Outro', np.NaN , inplace=True)
    df.to_sql('vw_data_import', con=engine, if_exists='append', schema="public", index=False)
    return df

df_to_sql(source, engine, nrows)
